//import 'package:audioplayer/audioplayer.dart';
import 'package:just_audio/just_audio.dart';
import 'package:flutter/material.dart';
import 'package:wakelock/wakelock.dart';
import 'dart:developer' as developer;
import 'dart:async';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Fis-timer',
      theme: ThemeData(
        // This is the theme of your application.
        //
        // Try running your application with "flutter run". You'll see the
        // application has a blue toolbar. Then, without quitting the app, try
        // changing the primarySwatch below to Colors.green and then invoke
        // "hot reload" (press "r" in the console where you ran "flutter run",
        // or simply save your changes to "hot reload" in a Flutter IDE).
        // Notice that the counter didn't reset back to zero; the application
        // is not restarted.
        primarySwatch: Colors.blue,
      ),
      home: const MyHomePage(title: 'Fis-Timer'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key, required this.title}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;
  int _seconds = 0;
  int _minutes = 0;

  var _modeTimes = [1, 1];

  bool _active = false;
  bool _pause = false;

  String _text = "00 : 00";
  String _btnStartText = 'Start';
  String _currentMode = '1 - 1';

  Timer? _timer = null;

  Color _bgColor = Color.fromARGB(255, 217, 217, 217);

  //AudioPlayer audioPlayer = new AudioPlayer();
  AudioPlayer? _player = null;
  //AudioPlayer _player = AudioPlayer();

  _MyHomePageState() {
    try {
      _player = AudioPlayer();
    } catch (e) {}
  }

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  void _startTimer() {
    if (_timer == null) {
      _timer = new Timer.periodic(Duration(seconds: 1), _timeout);
      Wakelock.enable();
      onActive();
      setState(() {
        _btnStartText = 'Pause';
        _currentMode =
            _modeTimes[0].toString() + ' - ' + _modeTimes[1].toString();
      });
    } else if (!_pause) {
      _pause = true;
      Wakelock.disable();
      setState(() {
        _btnStartText = 'Resume';
      });
    } else {
      _pause = false;
      Wakelock.enable();
      setState(() {
        _btnStartText = 'Pause';
      });
    }
  }

  void _resetTimer() {
    try {
      _timer?.cancel();
    } catch (e) {}

    _timer = null;
    _active = false;
    _pause = false;
    _minutes = _seconds = 0;

    setState(() {
      _btnStartText = 'Start';
      _text = "00 : 00";
      _bgColor = Color.fromARGB(255, 217, 217, 217);
    });
  }

  void _timeout(Timer t) {
    if (_pause) return;

    setState(() {
      if (_seconds == 59) {
        _seconds = 0;

        if (_minutes == 59) {
          _minutes = 0;
        } else {
          _minutes++;
        }
      } else {
        _seconds++;
      }
      String mm =
          (_minutes < 10) ? ("0" + _minutes.toString()) : (_minutes.toString());
      String ss =
          (_seconds < 10) ? ("0" + _seconds.toString()) : (_seconds.toString());
      _text = mm + " : " + ss;
    });

    _counter--;

    if (_counter <= 0) {
      if (_active)
        onRest();
      else
        onActive();
    }
  }

  void updateCurrentMode() {
    setState(() {
      _currentMode =
          _modeTimes[0].toString() + ' - ' + _modeTimes[1].toString();
    });
  }

  void playSoundStart() async {
    try {
      if (_player != null) {
        await _player!.setAsset("assets/audio/Start.mp3");
        await _player!.play();
      }
    } catch (e) {}
  }

  void playSoundStop() async {
    try {
      if (_player != null) {
        await _player!.setAsset("assets/audio/Stop.mp3");
        await _player!.play();
      }
    } catch (e) {}
  }

  void onActive() {
    _active = true;
    _counter = _modeTimes[0] * 60;
    playSoundStart();

    setState(() {
      _bgColor = Color.fromARGB(255, 201, 193, 240);
    });
  }

  void onRest() {
    _active = false;
    _counter = _modeTimes[1] * 60;
    playSoundStop();

    setState(() {
      _bgColor = Color.fromARGB(255, 236, 195, 195);
    });
  }

  @override
  Widget build(BuildContext context) {
    try {
      //_player = AudioPlayer();
    } catch (e) {}
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      backgroundColor: _bgColor,
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
        centerTitle: true,
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'Time.',
            ),
            Text(
              //'$_minutes : $_seconds',
              _text,
              style: Theme.of(context).textTheme.headline4,
            ),
            /* FloatingActionButton(
              onPressed: _startTimer,
              tooltip: 'Start',
              child: const Icon(Icons.add),
            ),
            FloatingActionButton(
              onPressed: _resetTimer,
              tooltip: 'Reset',
              child: const Icon(Icons.add),
            ), */
            TextButton(
                onPressed: _startTimer,
                child: Text(_btnStartText),
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.red))))),
            TextButton(
                onPressed: _resetTimer,
                child: const Text('Reset'),
                style: ButtonStyle(
                    shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                        RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(18.0),
                            side: BorderSide(color: Colors.red))))),
            ListTile(
                title: new Center(child: new Text('1:1')),
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5)),
                onTap: () {
                  if (_timer == null) {
                    _modeTimes[0] = 1;
                    _modeTimes[1] = 1;
                    updateCurrentMode();
                  }
                }),
            ListTile(
                title: new Center(child: new Text('2:1')),
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5)),
                onTap: () {
                  if (_timer == null) {
                    _modeTimes[0] = 2;
                    _modeTimes[1] = 1;
                    updateCurrentMode();
                  }
                }),
            ListTile(
                title: new Center(child: new Text('3:1')),
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5)),
                onTap: () {
                  if (_timer == null) {
                    _modeTimes[0] = 3;
                    _modeTimes[1] = 1;
                    updateCurrentMode();
                  }
                }),
            ListTile(
                title: new Center(child: new Text('5:1')),
                shape: RoundedRectangleBorder(
                    side: BorderSide(color: Colors.black, width: 1),
                    borderRadius: BorderRadius.circular(5)),
                onTap: () {
                  if (_timer == null) {
                    _modeTimes[0] = 5;
                    _modeTimes[1] = 1;
                    updateCurrentMode();
                  }
                }),
            Text(
              //'$_minutes : $_seconds',
              _currentMode,
              style: Theme.of(context).textTheme.headline4,
            ),
            /* RaisedButton(
              onPressed: () {},
              color: Colors.amber,
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(10)),
              child: Text("Click This"),
            ) */
          ],
        ),
      ),
      /* DropdownButton<String>(
        value: _currentMode,
        onChanged: (String? value) {
          setState(() {
            _currentMode = value!;
          });
        },
        items: _modeList.map((mode) {
          return DropdownMenuItem(
            child: new Text(mode),
            value: mode,
          );
        }).toList(),
      ), 
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      )*/ // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
